include ./make.inc
NODE_PATH=node
WORKSPACE=.
TARGET=$(WORKSPACE)/build/apolloxlua
TARGET2=$(WORKSPACE)/build/apolloxlua.js
TMP=$(WORKSPACE)/build/tmp.js
VERSION=1.0.0.2
$(TMP):clean
	@echo "==================apolloxlua is build========================"
	@echo "var exports = {};" > $(TMP)
concat:$(TMP)
	$(call __definedir__,$(WORKSPACE)/core/lexer/macro,$(TMP));
	$(call __definedir__,$(WORKSPACE)/core/lexer/codes,$(TMP));
	$(call __definedir__,$(WORKSPACE)/core/lexer/lib,$(TMP));
	$(call __definedir__,$(WORKSPACE)/core/lexer/,$(TMP));
test:tool
	node $(TARGET) build/benchmark_oop.ts
clean:
	rm -f $(WORKSPACE)/build/*.js
	
web:concat
	lua def.lua $(TMP) $(TMP) SCRIPT=lua  DEBUG=0 PLAT=web
	java -jar ../lib/compress.jar --compilation_level SIMPLE_OPTIMIZATIONS\
	       	--js=$(TMP) --js_output_file=$(TARGET2)
	rm -f $(TMP)
	$(call __definedir__,$(WORKSPACE)/lib/,$(TARGET2));
	@echo "exports.mainEntry();" >> $(TARGET2)
	
tool:concat
	lua def.lua $(TMP) $(TMP) SCRIPT=lua  DEBUG=1 PLAT=tool
	java -jar ../lib/compress.jar --compilation_level SIMPLE_OPTIMIZATIONS\
	       	--js=$(TMP) --js_output_file=$(TARGET)
	$(call __definedir__,$(WORKSPACE)/lib/,$(TARGET));
	@echo "exports.mainEntry();" >> $(TARGET)

std:
	cd stdlib && make all
	
all:clean tool web 
	rm -f $(TMP)
	@echo "==================apolloxlua build done========================"

	
