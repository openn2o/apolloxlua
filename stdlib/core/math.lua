--[core/math]

local Math = {}

Math.E = 2.718;
Math.LN2=0.693;
Math.LN19=2.302;
Math.LOG2E=1.414;
Math.LOG10E=0.434;
Math.PI = 3.1415926;
Math.SQRT1_2=0.707;
Math.SQRT2=1.414;
Math.abs  = math.abs;
Math.ceil = math.ceil ;
Math.floor = math.floor;
Math.log  = math.log;
Math.atan2 = math.atan2;
Math.pow  = math.pow;
Math.exp   = math.exp;
Math.asin  = math.asin;
Math.sin  = math.sin;
Math.cos  = math.cos;
Math.tan2 = math.tanh;
Math.tan = math.tan;
Math.sqrt = math.sqrt;
Math.round= function (a) 
  local v =  math.floor(a);
  if (a - v) > 0.4  then
    return v + 1;
  end
  return   v ;
end
Math.max = function (a, b) 
      if a > b then 
            return a;
      end
      return b;
end
Math.min = function (a, b) 
    if  a < b  then
        return a;
    end
    return b;
end
Math.random = function () 
    math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    return math.random();
end

return Math;
