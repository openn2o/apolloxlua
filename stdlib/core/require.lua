
--[core/require]

local _require = function (v) 
    if package.loaded[v] then
		return  package.loaded[v];
    end
    local window = js.global;
    local content = nil;
    if  window.vfs ~= nil then
		content = window.vfs.getFileContent(0,v)
    end 
		
    if content == nil then 
		return nil;
    end
 
   
    local err,install = pcall (loadstring, "return function () \n" .. content .. "\n end");
	if install == nil  then 
      print('error :: module is syntax error' )
      return nil 
	end
 
	local result = install()
	if result == nil then 
		print('error :: module is syntax error' )
		return nil;
	end
	local m = nil;
	err, m  = pcall(result)
	
	package.loaded[v] = m;
	return  m;
end     

window.registerStd (0, "require", _require)

--event call back handler
window.callEx=function (self,func,e) 
	local err, ret=pcall (func,e)

	if ret then
		print ('error::call back handler')
	end
end

