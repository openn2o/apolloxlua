/***
 * 
 */
;exports[{*blocks.ObjectExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
     var probs = eax.properties;
                    
     if(probs.length == 0) { /// new Object  
            return "{}";
      } else { 
            ///{a=>b}
            var len  = probs.length;
            var arr  = [];
            
            for( var i = 0; i< len; i++) {
                arr.push(GetValueByType(probs[i].key));
                arr.push(GetValueByType(probs[i].value));
            }

            var code= [];
            var k,v;
            while (!arr.length == 0) {
                k  = arr.shift();
                v  = arr.shift();
                if(!k) {
                    continue;
                }
                if(k.indexOf("\"") == -1) {
                      code.push("[\""+k+"\"] = "+v);
                } else {
                      code.push("["+k+"] = "+v);
                }
            }
            
            var smt = "{\n" + code.join(",\n") + "\n}";
            return smt;
        }
{% end %}
}

