/***
 * 
 */
;exports[{*blocks.ArrayExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
           var e = eax;
           var isExtern = 0;
           depencies["array"] = true;
           if(e && e.elements) {
                var len = e.elements.length ;
                if(len == 0) { /// []
                    return "{}";
                } else {
                    {% if USE_AOP then %}
                        if(len==1) {
                            console.log("extern syntax");
                            var  test_method = e.elements[0];
                            if(test_method.type == "CallExpression" &&
                             extensionSyntax[test_method.callee.name]) { ///is extension syntax
                                  test_method.type = extensionSyntax[test_method.callee.name];
                                  isExtern = 1;
                             }
                        }
                    {% end %}
                    var codes = [];
                    for( var i=0; i< len; i++) {
                        codes.push(GetValueByType(e.elements[i]));
                    }
                    var len = codes.length;
                    var codeN = [];
                    for( var i = 1; i<= len ; i++) {
                        if(i == 1) {
                             codeN[i] = "["+i+"]="+codes[i-1];
                        } else {
                             codeN[i] = "\n["+i+"]="+codes[i-1];
                        }
                    }
                    codeN.shift();
                    if(isExtern == 1) {
                        if(aop_retval) {
                            return aop_retval.join("");
                        }
                        return "";
                    }
                    return   "{"+codeN.join(",") +"}"
                }
            }
            
            return "null";
{% end %}
}