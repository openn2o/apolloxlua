/***
 * 
 */
;exports[{*blocks.FunctionExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
        var e = eax;
        var params = [];
        var isrestParams = 0;
        if(e.params) {
            var len = e.params.length;
            if(len > 0) {
                for(var i = 0; i < len; i++)  {
                    params[i] = GetValueByType(e.params[i])
                }
            }
            if(len == 1) {
               if(e.params[0].type == "RestElement") {
                   ///可变长数组
                   isrestParams = 1;
                   e.params[0] = e.params[0].argument.name ;
               }
            }
        }
        
        if(eax.id && eax.id == "__classMethod") {
            if(eax.methodName) {
                if(eax.methodName == currClassName) {
                    constructMethods = {};
                    constructMethods.params = params;
                    constructMethods.code   = GetValueByType (e.body);
                    return "";
                }
                params.unshift("self")
            }
        }
       
        if(isrestParams) {
            var code  = "function ("+params.join(",")+") \n";
            code += "local " + e.params[0] + "=arg or ...\n"
        } else {
            var code  = "function ("+params.join(",")+") \n";
        }
        if(e.body) {
            var curr_code = GetValueByType (e.body);
            if(aop_method_hook) {
                aop_blocks.push(curr_code)
                aop_params  = params;
            } 
            code += curr_code; 
        }
        code += "\nend\n";
        return code;
{% end %}
}

