/***
 * 
 */
;exports[{*blocks.MethodDefinition*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}

   var key = GetValueByType(eax.key);
   
   var isstatic = Number(eax['static']) >> 0;
   eax.value.id = "__classMethod";
   eax.value.methodName = key;
   if(isstatic == 1) {
        eax.value.id = "__classStaticMethod";
   }
   var body =  GetValueByType(eax.value);
   if (currClassName == key) {
       return "";
   }
   var code =  currClassName + "." + key +" ="+ body
   
   //// static method
   return code;
   //console.log(key, isstatic, body) ;
{% end %}
}