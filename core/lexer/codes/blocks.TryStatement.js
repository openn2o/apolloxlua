/***
 * 
 */
;exports[{*blocks.TryStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
            var e = eax;
            var try_block = "";
            if(e.block) {
                try_block = GetValueByType(e.block);
            }
            
            var catch_block = "";
            if(e.handler) { //catch 
                catch_block = GetValueByType(e.handler);
            }
            
            var final_block = "";
            if(e.finalizer) {
                final_block = GetValueByType(e.finalizer);
            }
            
            var code = "local status,err = pcall (function()\n" +
            try_block + 
            "\nend)\n if err ~= nill then \n" + catch_block + "end \n"+
            final_block+"\n";
            
            return code;
{% end %}
}