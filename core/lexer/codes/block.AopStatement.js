{% if USE_AOP then %}
;exports[{*blocks.AopStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
   aop_retval = null;
// console.log("AOP STATEMENT");
   var fname =  GetValueByType(eax.callee);
   var arg   = eax.arguments;
   var len   = arg.length;
   var stack = [];
   
   aop_method_hook = true;
   
   aop_blocks =  [];
   aop_params =  [];
   for(var i =0; i< len; i++) {
         stack.push(GetValueByType(arg[i]));
   }
   
   if(stdLibSymblos[fname]) {
       fname = stdLibSymblos[fname] (fname);
   }
   
   var member = fname.split(".");
   if(member.length > 1) {
       if(stdLibSymblos[member[1]]) {
            fname = stdLibSymblos[member[1]](
                member[0],
                stack
            );
            return fname;
       }
   }
   
   aop_method_hook = false;
   /// 0 type 1 name 2 func;
   if(stack.length == 3) {
       var reg = new RegExp("\"","g");
       var type= stack[0].replace(reg,"");
       var name= stack[1].replace(reg,"");
       var func= stack[2];
       
       switch (type) {
           case "breakpoint":
           aop_breakpoint[name] = {};
           aop_breakpoint[name] ["type"] = type;
           aop_breakpoint[name] ["code"] = aop_blocks;
           aop_breakpoint[name] ["params"]=aop_params;
           console.log("add breakpoint", aop_breakpoint[name])
           break;
       }
   }
   return null;
{% end %}
}
{% end %}
