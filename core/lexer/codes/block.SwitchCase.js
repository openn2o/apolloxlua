/***
 * 
 */
;exports[{*blocks.SwitchCase*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
            var e = eax;
            var test;
            if(e.test) {
                test =  GetValueByType(e.test);
                exports.test_switch.push(test);
            }
            var codes = [];
             
            if(e.consequent) {
                 var consequent = e.consequent;
                 var len = consequent.length;
                 for(var i =0;i< len;i++) {
                     codes.push(GetValueByType(consequent[i]));
                 }
            }
            var code  = "";
            var len   = exports.stack_switch.length;

            if(!exports.stack_switch[0]) {
                console.log("[error] switch stack error.");
            }
            
            if(!test) {
                 
                 code += "exports[\""+exports.defaultName+"\"]=function ()\n";
                 
                 code +=  codes.join("\n");
                 
                 code +=  "end\n"
                 
                 
            } else {

                 
                 var label = exports.stack_switch.pop();
                 exports.switches[test+""] = label;
                 if(codes.length == 0) {
                     exports.merge_switch.push(label);
                     return "";
                 }
                 
                                  
                 
                 code += "exports[\"" + label +"\"]=function ()\n";
                 if(len-1 < 0) {
                    var next  = exports.stack_switch[0];
                 }else {
                    var next  = exports.stack_switch[exports.stack_switch.length-1];
                 }
            
//               if(exports.merge_switch.length != 0) {
//                   test = exports.merge_switch.join("&&") + "&& $istrue("+exports.stack_switch[0]+"!="+test+")";
//                   exports.merge_switch = [];
//               } else {
//                   test = "$istrue("+exports.stack_switch[0]+"!="+test+")";
//               }
                 code += codes.join("\n");
                 
                 code += "end\n"
                 
                 
                 
                 if(exports.merge_switch.length > 0){
                     var len = exports.merge_switch.length;
                     for(var i = 0 ; i< len; i ++ ) {
                         var need = exports.merge_switch[i];
                         code += "exports[\"" + need +"\"]=" + "exports[\"" + label +"\"]\n";
                     }
                     exports.merge_switch = [];
                 }
//               code += test + " && $goto("+next+");\n " + codes.join("");
            }
           
            
            return code;
{% end %}
}

