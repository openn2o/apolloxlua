/***
 * 
 */
;exports[{*blocks.BinaryExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
  var left = GetValueByType(eax.left);
  var right= GetValueByType(eax.right);
  
  switch (eax.operator) {
        case "!=":
        eax.operator = "~=";
        break;
        case "+":
        var b1 = isString(left);
        var b2 = isString(right);
        if(eax.left.type == "Identifier" ) {
            eax.operator = "..";
            
            if(arraySymblos[left] && arraySymblos[right]) {
                eax.operator = "+";
                arraySymblos[left] = 1;
            }
            
        } else if(eax.right.type == "Identifier") {
            eax.operator = "..";
            if(arraySymblos[right]) {
                eax.operator = "+";
                arraySymblos[right] = 1;
            }
        } else if( b1 || b2 ) {
            eax.operator = "..";
        } 
        break;
        case "-":
        case "*":
        case "/":
        case "%":
        if(eax.right.type == "Identifier") {
            arraySymblos[left] = 1;
        } else {
            arraySymblos[right] = 1;
        }
        arraySymblos[left + eax.operator + right] = 1;
        break;
        case ">>>":
        var b1 = isNumber(right);
        if(b1) {
            break;
        }
        var test = new RegExp("New[ \(]+","g");
        if(test.test(right)) {
            right = right.replace("New","new");
        }
        eax.operator = ":";
        break;
  }
  
  if(right == "null") {
        right = "nil"
  }
  return  left + eax.operator + right;
{% end %}
}

