/***
 * 
 */
;exports[{*blocks.UnaryExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
         var e = eax;
         switch (e.operator) {
               case "!":
               e.operator = "not";
               break;
         }
        return e.operator +" "+ GetValueByType(
                              e.argument
                        );
{% end %}
}

