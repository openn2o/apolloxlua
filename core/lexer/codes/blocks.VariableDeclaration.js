/***
 * 
 */
;exports[{*blocks.VariableDeclaration*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}

var ebx = eax.declarations;
var len = ebx.length;
var arr = [];
for(var i=0; i< len; i++) {
    arr[i] = GetValueByType(ebx[i]);
    
    if(len > 1) {
        arr[i] = arr[i].replace("local", "");
    }
}


if(len > 1) { ////多个定义 local a,b,c
    return "local " + arr.join(",") 
} 

return  arr[0];

{% end %}
}