/***
 * 
 */
;exports[{*blocks.ConditionalExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
            var e = eax;
            var code = "";
            var test = "";
             if(e.test) {
                test  =  GetValueByType(
                e.test);  
            }
            var consequent; 
            if(e.consequent) {
                consequent  =  GetValueByType(
                e.consequent);  
            }
            
            var alternate;
            
            if(e.alternate) {
                alternate  =  GetValueByType(
                e.alternate);  
            }
            // a>b ? a:b
            code = "("+test + " and " + consequent + " or " + alternate+")";
            return code;
{% end %}
}

