/***
 * 
 */
;exports[{*blocks.ReturnStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
     var ret  = GetValueByType(eax.argument);
     
     if(ret) {
           return "\nreturn " + ret +"\n";
     }
     
     return "\nreturn\n";
   
{% end %}
}
