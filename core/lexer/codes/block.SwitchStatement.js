/***
 * 
 */
;exports[{*blocks.SwitchStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
         var e   = eax;
         var str = "";
         exports.merge_switch = [];
         exports.test_switch  = [];
         exports.switches ={};
         exports.defaultName = "";
         depencies["switch"] = true;
         var sign ; 
            if(e.discriminant) {
                sign =  GetValueByType(e.discriminant);
                exports.stack_switch = [];
                exports.stack_switch.push(sign);
            }
            
            if(e.cases) {
                var cases = e.cases;
                var len = cases.length;
                var defaultName = getRandomName();
                exports.defaultName = defaultName;
                for(var i = 0; i< len; i++) {
                    exports.stack_switch.push(getRandomName());
                }
                
                exports.switches["\"default\""] = defaultName;
                var code= [];
                for( var i=0; i< len; i++) {
                    code[i] = GetValueByType(
                    cases[i]);
                }
            }
            var table_name = getRandomName()
            str = "local exports=exports\n"
            str += "local "+table_name + " = "+ makeLuaTable(getRandomName(),exports.switches) + "\n"
            str += "if exports[\""+exports.defaultName+"\"] == nil then\n"
            +code.join("") + "\nend\n";
            var len = exports.stack_switch.length;
            if(len == 0) {
                ///
            } else {
               //str += exports.stack_switch[1] + ":\n{}";
              str += "local __indexer__ = " + table_name + "["+sign+"]\n";
              str += "if __indexer__ == nil then \n __indexer__="+table_name+"[\"default\"]";
              str += "\nend\n"
              str += "if exports[__indexer__] ~= nil then \n exports[__indexer__]() \nend" 
            }
            
            ///pop value
            //exports.stack_switch.pop();
            
            if(exports.stack_switch.length != 0) {
                exports.stack_switch = [];
            }
            return str;
{% end %}
}

