/***
 * 
 */
;exports[{*blocks.MemberExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
        var target = GetValueByType(eax.object);
        var memeber= GetValueByType(eax.property);
        
        
        if(eax.computed) {
            return  target + "[" + memeber + "]";
        }
        
        if(stdLibProbs[memeber]) {
            return stdLibProbs[memeber](target);
        }
        return  target + "." + memeber;
        
{% end %}
}

