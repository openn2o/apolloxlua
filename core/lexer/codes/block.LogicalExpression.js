/***
 * 
 */
;exports[{*blocks.LogicalExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}

     var e = eax;
     
     var left  = GetValueByType( e.left);
            
     right     = GetValueByType(e.right);
                    
                     
     switch (e.operator) {
            case "&&":
            e.operator = " and ";
            break;
            case "||":
            e.operator = " or ";
            break;
            
    }
     
    return left + e.operator + right;
{% end %}
}