{% if USE_AOP then %}
;exports[{*blocks.BreakPointStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
   aop_retval = [];
   var fname = GetValueByType(eax.callee);
   var arg   = eax.arguments;
   var len   = arg.length;
   var stack = [];
   
   for(var i =0; i< len; i++) {
         stack.push(GetValueByType(arg[i]));
   }
   if(stdLibSymblos[fname]) {
       fname = stdLibSymblos[fname] (fname);
   }
   var member = fname.split(".");
   if(member.length > 1) {
       if(stdLibSymblos[member[1]]) {
            fname = stdLibSymblos[member[1]](
                member[0],
                stack
            );
            return fname;
       }
   }
   
   /// 1 name
   if(stack.length >=  1) {
      var reg = new RegExp("\"","g");
      var name= stack[0].replace(reg,"");
      if(aop_breakpoint[name]) {
          var esi = aop_breakpoint[name];
          
          var dsp = [];
          if(esi.params.length > 0) {
              var ulen = esi.params.length;
              for(var u= 0; u < ulen; u ++) {
                  dsp.push("local " + esi.params[u] + "=" + stack[u+1] + "\n");
              }
          }
          aop_blocks = esi.code ;
          aop_retval = dsp.concat(aop_blocks);
      }
   }
   return null;
{% end %}
}
{% end %}
