/***
 * 
 */
;exports[{*blocks.ClassBody*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}

        var ebx = eax.body;
        if(ebx) {
            var len = ebx.length;
            var codes = [];
            for(var i =0; i < len; i++) {
                 codes[i] = (GetValueByType(ebx[i]))
            }
            return codes.join("\n");
        }

{% end %}
}