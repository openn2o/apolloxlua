/***
 * 
 */
;exports[{*blocks.IfStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}

            var e = eax;
            var codes = [];
            
            if(e.test) {
                var test  =  GetValueByType(e.test);  
                codes.push("\nif " +test+ " then\n");
            }
            
            if(e.consequent) {
                codes.push(GetValueByType (e.consequent));
            }
            //else 
            if(e.alternate) {
                var elsem = "\n else \n" + GetValueByType (e.alternate);
                codes.push(elsem);
                codes.push("\nend\n");
            } else {
                 codes.push("\nend\n");
            }
           
            return codes.join("");

{% end %}
}

