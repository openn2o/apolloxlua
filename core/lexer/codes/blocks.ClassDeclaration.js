/***
 * 
 */
;exports[{*blocks.ClassDeclaration*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}


        var className = GetValueByType(eax.id);
        currClassName = className;
         
        if(classesSymblos[className]) {
            console.log("alerdy define classed.");
        }
        var supperClass = GetValueByType(eax.superClass);
       
        
        if(supperClass == className) {
            console.log("error::class can't self examination");
            return ;
        }
        
        if(eax.body) {
            var body = GetValueByType(eax.body);
        }
         
        if(supperClass) {
            var code = "local " + className + "="+supperClass+":new()\n"
        } else {
            var code = "local " + className + "={}\n"
        }
        if(constructMethods) {
            code += "function " + className + ":new(" + 
            constructMethods.params.join(",") + ")\n"
        } else {
            code += "function " + className + ":new(o)\n"
        }
        
        code += "local o=o or {}\n"
        code += "setmetatable(o,self)\n" 
        if(constructMethods) {
            code+= constructMethods.code + "\n";
        }
        code += "self.__index= self\n"
        code += "return o\n"
        code += "end\n"
        
        {% if  PLAT  == "web" then %}
        code += "\n";
        code += "window.registerStd (0, \""+className+"\", "+className+")\n"
        {% end %}
        
        body = code + body;
        constructMethods = null;
        currClassName    = null;
        

        
        
        return body;
{% end %}
}