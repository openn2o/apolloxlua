/***
 * 
 */
;exports[{*blocks.CallExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
   var fname =  GetValueByType(eax.callee);
               
   var arg   = eax.arguments;
   var len   = arg.length;
   var stack = [];
   
   for(var i =0; i< len; i++) {
         stack.push(GetValueByType(arg[i]));
   }
   if(stdLibSymblos[fname]) {
       fname = stdLibSymblos[fname] (fname, arg);
   }
   var member = fname.split(".");
   if(member.length > 1) {
       if(stdLibSymblos[member[1]]) {
            fname = stdLibSymblos[member[1]](
                member[0],
                stack
            );
            return fname;
       }
   }
   return fname + " (" + stack.join(",") + ")"
{% end %}
}

