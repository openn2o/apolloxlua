/***
 * 
 */
;exports[{*blocks.BlockStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
         var e = eax;
         var codeblock = e.body;
         var len  = codeblock.length;
         
         var buff = [];
         for( var i = 0; i< len; i++ ) {
               buff[i] = GetValueByType (codeblock[i]);
         }
         return buff.join("\n");
{% end %}
}

