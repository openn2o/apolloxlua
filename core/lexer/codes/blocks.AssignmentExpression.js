/***
 * 
 */
;exports[{*blocks.AssignmentExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
  
  var left = GetValueByType(eax.left);
  var right= GetValueByType(eax.right);
  
  switch (eax.operator) {
      case "+=":
      return left +"=" +left + "+" +right;
      case "-=":
      return left +"=" +left + "-" +right;
      case "*=":
      return left +"=" +left + "*" +right;
      case "/=":
      return left +"=" +left + "/" +right;
      case "%=":
      return left +"=" +left + "%" +right;
      case "=":
      if(arraySymblos[right] && right.indexOf("\"") == -1) {
            arraySymblos[left] = 1;
      } else {
            arraySymblos[left] = 0;
      }
      break;
  }
  
  return  left + eax.operator + right;
{% end %}
}
