{% if  PLAT  == "web" then %}
   var apolloxLua = {
       "parse":exports.Main
   }
   
   exports.mainEntry = function (args) {
       console.log("entry web mode.");
   }
{% end %}

{% if  PLAT  == "tool" then %}
exports.mainEntry = function () {
  var args = process.argv;
  try {
          if(args.length > 0) {
            var path_need_com = args[2];
            var type =  args[3];
            exports.path_need_com =  args[2] + ".lua"
            if(type=="window"){
                FiberTask.prototype.onTaskDone = function (parma, buff){
                    if( buff && parma.name == "type" ){
                        exports.Main(buff.toString("utf8"));
                    }
                }
                var t = new fiber.FiberTask("type " + path_need_com,{"name":"type"});
                var r = new FiberExecute();
                r.add(t);
                r.start();
            } else {
                FiberTask.prototype.onTaskDone = function (parma, buff){
                    if( buff && parma.name == "cat" ){
                        exports.Main(buff.toString("utf8"));
                    }
                }
                var t = new fiber.FiberTask("cat " + path_need_com,{"name":"cat"});
                var r = new FiberExecute();
                r.add(t);
                r.start();
            }
            return;
          }
          process.exit(0);
  }catch (e) {
          console.log(e.toString());
          process.exit(1);
  }
}

{% end %}