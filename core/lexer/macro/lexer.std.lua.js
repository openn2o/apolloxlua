
{-inline_lua_std-}

if(!isInclude) {
    var needRequire = false;
    var g = gen_codes.join("\n");
    ////stdlib include
    if(depencies["switch"]) {
        gen_codes.unshift("local exports={}");
        depencies["switch"] = null;
    }
    ///math
    if(g.indexOf("Math.") != -1) {
        gen_codes.unshift("local Math  = require(\"core/math\")");
        needRequire = 1;
    }
    ///aray
    if(depencies["array"]) {
        gen_codes.unshift("local table = require(\"core/array\")");
        depencies["array"] = null;
        needRequire = 1;
    }
    
    ////end
    
    for(var k in depencies) {
        if(depencies[k]) {
            gen_codes.unshift("require(\""+k+"\")");
        }
    }
}

depencies = {};
arraySymblos = {};
classesSymblos={};
{-inline_lua_std-}

{% if  SCRIPT  == "lua" then %}

var isInclude = false;
////stdlib add 
var stdLibSymblos = {
      "include":function (state, params) {
      var fileName = params[0].value;
      
      if(include_deps[fileName]) {
          return '-- already exists ';
      }
      
      include_deps[fileName] = {};
      
      {% if  PLAT  == "web" then %}
           include_deps[fileName].code = vfs.getFileContent(fileName);
      {% end %}
      
      {% if  PLAT  == "tool" then %}
           var fs = require('fs');
           include_deps[fileName].code =  fs.readFileSync(fileName).toString("utf8");
      {% end %}
      
      var fileExtension = fileName.substring(fileName.lastIndexOf("."), fileName.length);
       
      var options = {
            attachComment: true,
            range: true,
            loc: true,
            sourceType: "script",
            tolerant: true
       };
        
       options.tokens = false;
       isInclude  = true;
       var result = exports["frontend"].parse(include_deps[fileName].code, options);
       var code   = exports.lexerGenerateCode(result);
       isInclude  = false;
       
       if(!include_deps[fileName].code) {
           {% if  PLAT  == "web" then %}
           console.error("vfs error:not find file " +fileName )
           include_deps[fileName] = null;
           {% end %}
       }
          
       switch (fileExtension) {
           case ".aop":
           return "-- include ";
           default:
           return "--not define"
       }
    },
    "console.log":function (state) {
        return "print";
    },
    "push":function (state, params) {
        return "table.apush(" +state + "," +  params.join(",") + ")";
    },
    "pop" :function (state, params) {
         return "table.apop(" +state + ")";
    },
    "unshift":function (state, params) {
        return "table.aunshift(" +state + "," +  params.join(",") + ")";
    },
    "shift":function (state, params) {
         return "table.ashift(" +state + ")";
    },
    "splice":function (state, params) {
        return "table.asplice(" +state + "," +  params.join(",") + ")";
    },
    "slice":function (state, params) {
        return "table.aslice(" +state + "," +  params.join(",") + ")";
    },
    "concat":function (state,params) {
         return "table.aconcat(" +state + "," +  params.join(",") + ")";
    },
    "join":function (state, params) {
        return "table.ajoin(" +state + "," +  params.join(",") + ")";
    },
    "eachIndex":function (state, params) {
        return "table.eachIndex(" +state + "," +  params.join(",") + ")";
    },
    "indexOf":function (state, params) {
        return "table.aindexOf(" +state + "," +  params.join(",") + ")";
    },
    "sort":function (state, params) {
        return "table.asort(" +state + "," +  params.join(",") + ")";
    }
}
/////class
var constructMethods = null;
var currClassName=null;
/////symblos
var classesSymblos = {};
var depencies    = {};
var arraySymblos = {};
var extensionSyntax = {
    "AOP":"AopStatement",
    "BREAKPOINT":"BreakPointStatement"
};
////
var stdLibProbs = {
    "length":function (state) {
        return "table.alength(" + state + ")"
    }
}
{% end %}