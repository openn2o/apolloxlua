 var gen_codes =[];
/****
 * 根据符号表生成代码
 * @param {Object} symblos
 */
exports.lexerGenerateCode = function (symblos) {
    
         var codes = symblos.body;
         var len = codes.length; 
         var eax = null;
         
         gen_codes =[];
         
         for(var i = 0; i< len; i++) {
             gen_codes[i] = GetValueByType(codes[i]);
             {% if  DEBUG  then %} 
             console.log("%d : %s",i + 1, gen_codes[i]);
             {% end %}
         }
        
        {% if  SCRIPT  == "lua" then %}
        ////stdlib add 
        {*blocks.inline_lua_std*};
        
        {% end %}
          
        {% if  PLAT  == "web" then %}
            var raw = gen_codes.join("\n");
            return  raw;
        {% end %}
        
        {% if  PLAT  == "tool" then %}
        var fs = require('fs');
        fs.open( exports.path_need_com, "w" ,"777", function () {
             fs.writeFileSync(exports.path_need_com , gen_codes.join("\n"));
        });
        {% end %}
        
}

// var global_scope = {}
// 
// var scope_stack = [];
// 
// scope_stack[0] = global_scope;
// 
// var currScope =  scope_stack[0];


var GetValueByType = function (value) {
        if(value== null) {
            return null;
        }
        
        if(!value.type) {
            console.log("type %s is not implements" , type);
            return  "not implements " + type;
        }
        
        if(exports[value.type]) {
             return exports[value.type](value);
        }
        return  null;
}
