{-inline_esprima_parse-}
    var options = {
        attachComment: false,
        range: false,
        loc: false,
        sourceType: "script",
        tolerant: true
    };
            
    options.tokens = false;
    var result = exports["frontend"].parse(buff, options);
{-inline_esprima_parse-}

/////
/////  生成code 
/////
{-inline_generate_code-}
    {% if  PLAT  == "web" then %}
         return  exports.lexerGenerateCode(result);
    {% end %}
    {% if  PLAT  == "tool" then %}
         exports.lexerGenerateCode(result);
    {% end %}
{-inline_generate_code-}
////
////   生成字节码
////

{-inline_generate_mid-}
 exports.lexerGenerateMidCode(result);
{-inline_generate_mid-}

{% if  SCRIPT  == "lua" then %}
exports.Main = function (buff) {
    {% if  DEBUG  then %}
        console.log("lua mode.")
    {% end %}
    if(buff.indexOf("new(") != -1 ) {
        console.log("error::syntax new syntax must be use New syntax")
        return ;
    }
    
    {% if  PLAT  == "web" then %}
        if(window.macroRender) {
            buff = window.macroRender(buff, window.macros);
        }
    {% end %}
    
    {*blocks.inline_esprima_parse*} 
    ///生成ast解析
    {*blocks.inline_esprima_parse*} 
    ///生成code
    {*blocks.inline_generate_code*}
}
{% end %}

{% if  SCRIPT  == "c" then %}
exports.Main = function (str) {
    {% if  DEBUG  then %}
        console.log("c mode.")
    {% end %}
    {*blocks.inline_esprima_parse*}
}
{% end %}


