{% if  PLAT  == "web" then %}
   ////虚拟文件系统支持
   var vfs = {};
   vfs.baseUrl = BaseURL;
   vfs.name = "";
   vfs.data = {};
   vfs.rawNames= {};
   
   vfs.trimURL = function (fileName) {
       if(!fileName) 
            fileName = "null";
       if(fileName.charAt(0) == "." && 
       fileName.charAt(1) == "/") {
           return fileName.substring(2, fileName.legnth).replace("//", "/");
       }
       return fileName;
   }
   vfs.events= {};
   vfs.mount = function (uri, func) {
         vfs.baseUrl = BaseURL;
         vfs.events[uri] = func;
         var cacheName = uri;
         
         uri     = vfs.getBaseRemotingURL(uri);
         var tty = vfs.getFileContent(cacheName);
         ///默认为1天过期
         if(tty) {
             var fnames = tty.split("|");
             var len    = fnames.length;
             
             var time = fnames[len -1];
             
             if(time ==  new Date().toDateString()) {
                 for(var i=0; i< len; i++) {
                      vfs.rawNames[fnames[i]] = 1;
                 }
             }
             vfs.events[cacheName] (cacheName);
             return;
         }
         setTimeout(function () {
                var oReq = new XMLHttpRequest();
                oReq.open("GET", uri , true);
                oReq.responseType = "arraybuffer";
                oReq.onerror = function (oEvent) {
                    console.log(uri)
                }
                oReq.onload = function(oEvent) {
                        var blob = new Blob([oReq.response], {type: "image/png"});
                        var str = URL.createObjectURL(blob);
                        
                        var image = new Image();
                        var pngdrive = new PNGDrive();
                        image.onload =  function () {
                            pngdrive.decode(image);
                            var file  = pngdrive.getFileAt(0);
                            var codes = pngdrive.utf8Decode(file.content);
                            var arr  = codes.split("--[");
                            var index= -1;
                            var fileName = "";
                            var n    = arr.length;
                            var d    = []
                            for(var i= 0; i<n;i++) {
                                var code = arr[i];
                                if(!code) continue;
                                index = code.indexOf("]");
                                if(index == -1) {
                                    continue;
                                }
                                fileName = code.substring(0, index);
                                d.push(fileName)
                                if(vfs) {
                                    vfs.setFileContent(fileName, code.substring(index+1, code.length))
                                }
                            }
                            if(vfs) {
                                vfs.setFileContent(cacheName, (d.join("|")) +"|"+ new Date().toDateString());
                                vfs.events[cacheName] (cacheName);
                            }
                        }
                        
                        image.onerror = function () {
                            console.log("not find std lib");
                        }
                        image.src=str;
                };
                oReq.send();
         },0)
   }
   
   vfs.getBaseRemotingURL = function (fileName) {
       if(!vfs.baseUrl) {
           return vfs.trimURL(fileName);
       }
       return vfs.baseUrl + "/" + vfs.trimURL(fileName) 
       + "?__vfs__=" + ((Math.random() * 10000) >> 0);
   }
      
   vfs.getNormalisePath = function (fileName) {
       return vfs.trimURL(fileName);
   }
   
   vfs.listFileNames = function () {
      var arr = [];
      for(var k in vfs.rawNames) {
          arr.push(k)
      }
      return arr;
   }
   
   vfs.getFileContent = function (fileName) {
       var fName = vfs.getNormalisePath(fileName) ;
       var fData = vfs.data[fName];
       
       if(!fData) {
           fData = window.localStorage.getItem(fName);
           if(!fData) {
               return null;///远程拉去
           } else {
               vfs.data[fName] = fData;
           }
       }
       return fData;
   }
   
   vfs.setFileContent = function (fileName, content) {
       vfs.rawNames[fileName] = true;
       var fName = vfs.getNormalisePath(fileName);
       vfs.data[fName] = content;
       window.localStorage.setItem(fName,content);
   }
{% end %}
