{% if  PLAT  == "web" then %}

setTimeout(function (fileName) {
    if(vfs) {
        vfs.mount("stdlib.rsl", function (fileName) {
            if(fileName == "stdlib.rsl") {
                var def = vfs.getFileContent("core/def");
                if(def) {
                     L.execute(def);
                }
                ///注册require函数
                var require = vfs.getFileContent("core/require");
                if(require) {
                     L.execute(require);
                }
            }
        });
    }
},50)

function registerStd(v, e) {
    L._G.set(v, e);
}
{% end %}