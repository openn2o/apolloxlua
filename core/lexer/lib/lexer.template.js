
{% if  PLAT  == "web" then %}
    /****
     *   template
     */
    var tmlCache = {};
    var keyMaxsize = 16;
    function getCacheKey (k) {
        var len = k.length;
        var key = '';
        if(len > keyMaxsize) {
            key =  k.substring(0,keyMaxsize-(len+"").length);
            key += k.charAt(len);
            key += len;
        } else {
            key = k;
        }
        
        return btoa(key);
    }
    
    function Tml(fmt, data) {
          var cacheKey = getCacheKey(fmt);
          var cache    = tmlCache[cacheKey];
          if(cache) {
              symblos = cache.symblos;
          } else {
              symblos = getSymblos(fmt, cacheKey);
              cache = tmlCache[cacheKey];
          }
          
          return replaceAll(symblos, fmt, data, cache.regexps)
    }

    function replaceAll (symblos, fmt, data, regs) {
           for (var k in symblos) {
               if(data[k]) {
                   fmt = fmt.replace(regs[k],  data[k]);
               } else {
                   fmt = fmt.replace(regs[k], "null");
               }
           } 
           return fmt;
    }

    function getSymblos(v, cacheKey) {
        var len = v.length;
        var sb = {};
        var k, c = '';
        var s = [];
        var reg = {};
        for ( var i = 0, j = 0; i < len; i ++ ) {
          c = v.charAt(i);
          if (c == '{') {
            s.push(1);
            k = '';
            continue;
          }
          if (c == '}') {
            j = s.pop();
            if (!sb[k] && j) {
              sb[k] = 1;
              reg[k] =  new RegExp('{' + k + '}', 'g')
            }
            k = ''; 
          }  
          k += c;
        }
        tmlCache[cacheKey] = {};
        tmlCache[cacheKey].symblos = sb;
        tmlCache[cacheKey].regexps = reg;
        return sb;
    }

{% end %}