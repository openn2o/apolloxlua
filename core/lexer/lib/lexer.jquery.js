{% if  PLAT  == "web" then %}
var dom = {}

dom.addEventListener =  function (el, type, handle) {
    $(el).on(type, lstate_lua_callback(handle))
}

dom.removeEventListener = function (el, type, handle) {
    $(el).off(type);
}

dom.find = function(str) {
    return $(str);
}

dom.setText = function (ele, str) {
    $(ele).text(str);    
}

dom.setHtml = function (ele, str) {
    $(ele).html(str);
}

dom.getJSObject = function () {
    return  {}
}

dom.tml     = function (str, data) {
    return Tml(str, data);
}

dom.time = function () {
   console.time("t");
}

dom.timeEnd = function () {
    console.timeEnd("t");
}

function lstate_lua_callback(f) {
    return function (e){
        window.callEx(f,e);
    }
}

{% end %}