local exports={}
local Person={}
function Person:new(o)
local o=o or {}
setmetatable(o,self)
self.__index= self
return o
end
Person.hellow =function (self) 
print ("base Person")
print (self.age)
end

Person.setAge =function (self,v) 
self.age=v
end

Person.staticSmethod =function (...) 
local args=arg or ...
print (args[1],args[2])
print ("i am static method")
end

local Person2=Person:new()
function Person2:new(name)
local o=o or {}
setmetatable(o,self)
self.name=name
print ("constructor..")
self.__index= self
return o
end

Person2.hellow =function (self) 
print ("override ")
print ("name="..self.name..",age="..self.age)
end

local p1=Person:new ()
local p2=Person2:new ("agent.zy")
p1:setAge (10)
p2:setAge (20)
p1:hellow ()
p2:hellow ()
Person2.staticSmethod (1)