
//[NameSpace("core")] 
class Person{
    hellow(){
         console.log("base Person")
		 console.log(this.age)
    }
	
	setAge(v) {
		this.age = v;
	}
	
	static staticSmethod (...args) {
		 console.log(args[1], args[2]);
		 console.log("i am static method");
	}
}
 
class Person2 extends Person{
	Person2 (name) {
		this.name = name;
		console.log("constructor..");
	}
	
    hellow(){
        console.log("override ");
		console.log("name=" + this.name + ",age=" + this.age);
    }
}

var p1 = Person  >>> New();
var p2 = Person2 >>> New("agent.zy");

p1 >>> setAge(10);
p2 >>> setAge(20);

p1 >>> hellow();
p2 >>> hellow();

Person2.staticSmethod(1)

