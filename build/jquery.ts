///////
//// web 模式下使用
//// JQuery 模块
/////

var window = require("core/window");
var Q  = {};
var dom= window.dom;

Q.find = function (query) {
    return dom.find(0, query);
}

Q.on = function (ele, type, handle) {
    dom.addEventListener(0, ele, type, handle);
}

Q.off= function (ele, type) {
	dom.removeEventListener(0, ele, type);
}

Q.text = function (ele, str) {
    dom.setText(0, ele, str );
}

Q.html = function (ele, str) {
    dom.setHtml(0, ele, str);
}


var ele = Q.find(".heading");
Q.off(ele, 'click');
Q.on(ele, 'click' ,  function (e) {
    console.log(e)
    
    var t = Q.find(e.target);
    
    Q.text (t, "karam is bitch")
    Q.html(t,"<a href='#'> for me </a>")
})

return Q;