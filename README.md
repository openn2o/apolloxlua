
## 项目介绍  [![star](https://gitee.com/openn2o/apolloxlua/badge/star.svg?theme=gray)](https://gitee.com/openn2o/apolloxlua/stargazers)

> apolloxlua 目前支持javascript到lua的翻译。可以在openresty和luajit里使用。这个工具分为两种模式， 一种是web模式，可以通过网页使用。另> 这里输入引用文本外一种是tool模式， 通常作为大规模翻译文件的工具使用。
 _agent.zy 于 18.07.21 晚_ 

## 特色

 1. 支持switch case 语句
 2. 支持trycatch 语句
 3. 可以自定义语法和后端实现
 4. 无缝对接lua标准库和三方库
 5. 支持array 和 数学库
 6. 支持面向对象开发 OOP
 7. 支持macro宏预处理 
 8. 支持面向切面开发 AOP

## 代码示例：
```c++
//[NameSpace("core")] 
class Person{
    hellow(){
         console.log("base Person")
		 console.log(this.age)
    }
	
	setAge(v) {
		this.age = v;
	}
	
	static staticSmethod (...args) {
		 console.log(args[1], args[2]);
		 console.log("i am static method");
	}
}
 
class Person2 extends Person{
	Person2 (name) {
		this.name = name;
		console.log("constructor..");
	}
	
    hellow(){
        console.log("override ");
		console.log("name=" + this.name + ",age=" + this.age);
    }
}

var p1 = Person  >>> New();
var p2 = Person2 >>> New("agent.zy");

p1 >>> setAge(10);
p2 >>> setAge(20);

p1 >>> hellow();
p2 >>> hellow();
```

## 如何扩展语法?
1可以使用luajit的ffi或swig扩展你的服务器。在语法分析里添加相应的标准库。 
2可以集成现有的lua库，包括但不限于resty\*和nginx-lua-\*。
3可以通过本工具的js语法，实现库的自举。


## 示例
[控制台示例][1]

![项目预览][2]

## 安装
下载后解压到你的目录， 通过控制台找到项目目录

> 生成web模式的js文件 `make web`
> 
> 生成工具模式的js文件 `make tool`
> 
> 生成两种模式文件 `make all`

##使用方式：

> web模式 : 在apolloxlua.js载入浏览器后使用一个全局的包装叫apolloxLua的变量包含一个parse的方法， 接受一个js代码串，返回lua代码串。
> tool模式: 使用的方式 node apolloxlua 输入文件 输出文件




  [1]: https://codepen.io/cncmd/pen/pZNZpp
  [2]: https://oscimg.oschina.net/oscnet/d1711d47c92a38053b0d3cb8577ecd97939.jpg
